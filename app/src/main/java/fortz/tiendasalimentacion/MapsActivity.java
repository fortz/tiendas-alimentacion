package fortz.tiendasalimentacion;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private static double lat;
    private static double lon;
    private Button add;
    private Button cancel;
    private Button ok;
    private Button yes;
    private Button no;
    private List<StoresService.Store> storeList = new ArrayList<>();
    private Marker newMarker;
    SharedPreferences mPrefs;
    final static String SAVED_STORES_NUMBER_PREF = "savedStoresNumber";
    final static String DATE_LAST_SAVED_PREF = "dateLastSaved";
    final static String VOTED_STORES_PREF = "votedStores";
    private static final int FINE_LOCATION_ACCESS_REQUEST = 0;
    private static final int MAX_STORES_TO_SAVE = 5;
    private static final long DAYS_TO_RESET_ADDING_STORES = 7;
    private HashMap<Marker, Integer> markersHashMap = new HashMap<>();
    private Integer storeId;
    private ArrayList<Integer> votedStores = new ArrayList();
    private static final int VOTES_TO_DELETE_STORE = -5;
    private static final int VOTES_TO_VERIFIE_STORE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtiene el SupportMapFragment y es notificado cuando el maa está listo para usarse.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Oculta el botón añadir en el onCreate
        Button fab = (Button) findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);

        lat = 0.0000000000;
        lon = 0.0000000000;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Oculta el botón añadir
                Button fab = (Button) findViewById(R.id.fab);
                fab.setVisibility(View.INVISIBLE);

                final Dialog dialog = new Dialog(MapsActivity.this);
                dialog.setContentView(R.layout.dialog_add);
                dialog.setTitle("Añadir tienda");

                dialog.show();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;

                // Rellena el EditText en el dialog
                final EditText latitudET = (EditText) dialog.findViewById(R.id.latitudeET);
                final EditText longitudET = (EditText) dialog.findViewById(R.id.longitudeET);
                final EditText storenameET = (EditText) dialog.findViewById(R.id.storeNameET);
                final EditText addressET = (EditText) dialog.findViewById(R.id.directionET);
                final EditText notesET = (EditText) dialog.findViewById(R.id.notesET);

                latitudET.setText(String.valueOf(lat));
                longitudET.setText(String.valueOf(lon));

                // Botón añadir
                add = (Button) dialog.findViewById(R.id.btanadd);
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (latitudET.getText().toString().equals("") || longitudET.getText().toString().equals("")) {
                            Context context = getApplicationContext();
                            CharSequence text = "¡Los campos latitud y longitud son obligatorios!";
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            latitudET.setText(String.valueOf(lat));
                            longitudET.setText(String.valueOf(lon));
                        } else {
                            // Comprueba si el usuario ha alcanzado el máximo número de tiendas a guardar
                            Date now = new Date();
                            Context context = getApplicationContext();
                            mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

                            // Suma 1 al número de tiendas añadidas por el usuario
                            SharedPreferences.Editor editor = mPrefs.edit();

                            // Obtiene el número actual de tiendas guardadas
                            Integer savedStores = mPrefs.getInt(SAVED_STORES_NUMBER_PREF, 0);

                            // Número máximo de tiendas alcanzado
                            if (savedStores == MAX_STORES_TO_SAVE) {

                                // Obtiene el último día que el usuario almacenó una tienda
                                long dateLastSavedMs = mPrefs.getLong(DATE_LAST_SAVED_PREF, 0);
                                Date dateLastSaved = new Date(dateLastSavedMs);

                                Duration dur = new Duration(new DateTime(dateLastSaved), new DateTime(now));
                                long d = dur.getStandardDays();

                                // Comprueba si el usuario puede almacenar tiendas de nuevo
                                if (d >= DAYS_TO_RESET_ADDING_STORES) {
                                    editor.putLong(DATE_LAST_SAVED_PREF, now.getTime());
                                    editor.putInt(SAVED_STORES_NUMBER_PREF, 1);
                                    editor.commit();

                                    // Añade la nueva tienda
                                    String storeName = storenameET.getText().toString();
                                    Double latitude = Double.valueOf(latitudET.getText().toString());
                                    Double longitude = Double.valueOf(longitudET.getText().toString());
                                    String address = addressET.getText().toString();
                                    String notes = notesET.getText().toString();

                                    addNewStore(storeName, latitude, longitude, address, notes, dialog);
                                    getStores();
                                } else {
                                    // Máximo de tiendas a añadir alcanzado
                                    CharSequence text = "¡Has registrado el máximo de tiendas esta semana!";
                                    int duration = Toast.LENGTH_SHORT;
                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                    dialog.dismiss();

                                    // Elimina el nuevo marcador
                                    if (newMarker != null){
                                        newMarker.remove();
                                    }
                                }
                                // Máximo número de tiendas no alcanzado
                            } else {
                                editor.putInt(SAVED_STORES_NUMBER_PREF, savedStores + 1);
                                editor.commit();

                                // Compruena si es la primera tienda añadida para el nuevo periodo y actualiza dateLastSaved
                                if (savedStores == 0) {
                                    editor.putLong(DATE_LAST_SAVED_PREF, now.getTime());
                                    editor.commit();
                                }

                                // Añade la nueva tienda
                                String storeName = storenameET.getText().toString();
                                Double latitude = Double.valueOf(latitudET.getText().toString());
                                Double longitude = Double.valueOf(longitudET.getText().toString());
                                String address = addressET.getText().toString();
                                String notes = notesET.getText().toString();

                                addNewStore(storeName, latitude, longitude, address, notes, dialog);
                                getStores();
                            }
                        }
                    }
                });

                // Botón de cancelar
                cancel = (Button) dialog.findViewById(R.id.btcancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Cancela la creación de nueva tienda
                        if (newMarker != null){
                            newMarker.remove();
                        }
                        dialog.dismiss();
                    }
                });
                dialog.getWindow().setAttributes(lp);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Recupera las tiendas y las almacena en una lista
        getStores();

        mMap = googleMap;

        // Establece la configuración del estilo del mapa usando el JSON almcenado en la carpeta "raw"
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_format));

        //Comprueba los permisos de la aplicación
        if(ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Muestra una explicación al usuario
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST);
        } else {
            if(!mMap.isMyLocationEnabled()){
                mMap.setMyLocationEnabled(true);
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMap.setBuildingsEnabled(true);
            }

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            Location myLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));


            if(myLocation == null){
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                String provider = locationManager.getBestProvider(criteria, true);
                myLocation = locationManager.getLastKnownLocation(provider);
            }

            if(myLocation != null){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom((new LatLng(myLocation.getLatitude(), myLocation.getLongitude())), 18), 1500, null);

                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        Criteria criteria = new Criteria();

                        if(ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST);
                            return true;
                        }
                        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));

                        if(location != null){
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 18), 1500, null);
                        }

                        return false;
                    }
                });

            }
        }


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            //Selecciona la posición para añadir y crear un marcador. Este se elimina al pulsar sobre otra posición.
            @Override
            public void onMapLongClick(LatLng coords) {
                //Muestra el botón de añadir cuando se hace una pulsación larga sobre el mapa
                Button fab = (Button) findViewById(R.id.fab);
                fab.setVisibility(View.VISIBLE);
                lon = coords.longitude;
                lat = coords.latitude;

                if (deleteMarker) {
                    marker.remove();
                }
                deleteMarker = false;
                marker = mMap.addMarker(
                        new MarkerOptions().position(
                                new LatLng(coords.latitude, coords.longitude)
                        ).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_add))
                );
                deleteMarker = true;
                newMarker = marker;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (newMarker != null){
                    newMarker.remove();
                }

                //Oculta el botón de añadir cuando se pulsa en cualquier otro lugar del mapa
                Button fab = (Button) findViewById(R.id.fab);
                fab.setVisibility(View.INVISIBLE);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                Boolean sameMarker = false;

                if (newMarker!= null){
                    if (newMarker.equals(marker)) {

                        Context context = getApplicationContext();
                        CharSequence text = "Este es la nueva tienda a añadir";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                        sameMarker = true;
                    } else {
                        // Elimina el nuevo marcador al hacer click sobre otro marcador
                        if (newMarker != null){
                            newMarker.remove();
                        }
                        //Esconde el botón de añadir al hacer click sobre otro marcador
                        Button fab = (Button) findViewById(R.id.fab);
                        fab.setVisibility(View.INVISIBLE);
                    }
                }
                if (!sameMarker){
                    showInfoDialog(marker);
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private Marker marker = null;
    Boolean deleteMarker = false;

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Aquí se controlan los clicks sonbre los elementos del menú lateral
        int id = item.getItemId();

        if (id == R.id.satellite_view) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        } else if (id == R.id.roadmap_view) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        } else if (id == R.id.help) {

            final Dialog dialog = new Dialog(MapsActivity.this);
            dialog.setContentView(R.layout.dialog_help);
            dialog.setTitle("Información");

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;

            ok = (Button) dialog.findViewById(R.id.btinfook);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void tryUpvoteStore(Integer id){
        // Marca una tienda como votada
        // Coge datos de las Shared Preferences
        Context context = getApplicationContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        Gson gson = new Gson();
        String votedStoresString = mPrefs.getString(VOTED_STORES_PREF, null);
        java.lang.reflect.Type type = new TypeToken<ArrayList<Integer>>(){}.getType();
        votedStores = gson.fromJson(votedStoresString, type);

        // Initializa el array la primera vez que una tienda es añadida
        if (votedStores == null) {
            votedStores = new ArrayList<>();
        }

        if (votedStores.contains(id)){
            // El usuario ya ha votado esta tienda
            // Toast
            CharSequence text = "Ya has votado esta tienda.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            //Votar negativamente una tienda
            upvoteStore(id);
            // Añade la tienda a las votedStores
            votedStores.add(id);

            // Convierte a String usando Gson
            votedStoresString = gson.toJson(votedStores);

            // Lo almacena en las Shared Preferences
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(VOTED_STORES_PREF, votedStoresString);
            editor.commit();
        }
    }

    public void tryDownvoteStore(Integer id){
        // Marca la tienda como votada
        // Coge datos de las Shared Preferences
        Context context = getApplicationContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        Gson gson = new Gson();
        String votedStoresString = mPrefs.getString(VOTED_STORES_PREF, null);
        java.lang.reflect.Type type = new TypeToken<ArrayList<Integer>>(){}.getType();
        votedStores = gson.fromJson(votedStoresString, type);

        // Initializa el array la primera vez que se añade una tienda
        if (votedStores == null) {
            votedStores = new ArrayList<>();
        }

        if (votedStores.contains(id)){
            // El usuarip ya ha votado esta tienda
            // Toast
            context = getApplicationContext();
            CharSequence text = "Ya has votado esta tienda.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            // Votar negativamente la tienda
            downvoteStore(id);
            // Añade una nueva tienda a votedStores
            votedStores.add(id);

            // Converti a String mediante Gson
            votedStoresString = gson.toJson(votedStores);

            // Guarda datos de las Shared Preferences
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(VOTED_STORES_PREF, votedStoresString);
            editor.commit();
        }
    }

    public void showInfoDialog(Marker marker){

        final Dialog dialog = new Dialog(MapsActivity.this);
        dialog.setContentView(R.layout.dialog_store_details);
        dialog.setTitle("Información");

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());


        Rect displayRectangle = new Rect();
        Window window = MapsActivity.this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        lp.width = ((int)(displayRectangle.width() * 0.9f));
        lp.height =((int)(displayRectangle.height() * 0.4f));

        final Marker m = marker;

        yes = (Button) dialog.findViewById(R.id.button_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sId = markersHashMap.get(m);
                tryUpvoteStore(sId);
                dialog.dismiss();
            }
        });

        no = (Button) dialog.findViewById(R.id.button_no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sId = markersHashMap.get(m);
                tryDownvoteStore(sId);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public void addNewStore(final String storeName, Double latitude, Double longitude, String address, String notes, final Dialog dialog){

        // Ajuste de la precisión de los decimales
        latitude = new BigDecimal(latitude).setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
        longitude = new BigDecimal(longitude).setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // Crea el logger
        // Establece el nivel de log deseado
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //Crea el servicio de retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StoresService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        StoresService.ChinoStores chinoStores = retrofit.create(StoresService.ChinoStores.class);

        StoresService.StoreAdd storeAdd = new StoresService.StoreAdd(storeName, latitude, longitude, address, notes);
        final Double finalLongitude = longitude;
        final Double finalLatitude = latitude;
        chinoStores.addStore(storeAdd).enqueue(new Callback<StoresService.StoreAddResponse>() {
            @Override
            public void onResponse(Call<StoresService.StoreAddResponse> call, Response<StoresService.StoreAddResponse> response) {
                // Toast
                Context context = getApplicationContext();
                CharSequence text = "Tienda añadida correctamente!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                // Añade una nueva tienda
                LatLng coords = new LatLng(finalLatitude, finalLongitude);
                Marker m = mMap.addMarker(new MarkerOptions().position(coords).title(storeName).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_validate)));

                if (storeId == null){
                    storeId = 0;
                }

                storeId = storeId + 1;
                markersHashMap.put(m, storeId);

                Log.v("Nueva tienda", markersHashMap.get(m).toString());

                // Elimina el antiguo marcador seleccionado
                if(newMarker != null){
                    newMarker.remove();
                }

                // Cierra el dialog
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<StoresService.StoreAddResponse> call, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Error al añadir la tienda!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    public void getStores(){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // Crea el logger
        // Establece el nivel de log deseado
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //Crea el servicio de retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StoresService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        StoresService.ChinoStores chinoStores = retrofit.create(StoresService.ChinoStores.class);

        Call<StoresService.StoreGet> call = chinoStores.getStores();
        call.enqueue(new Callback<StoresService.StoreGet>() {
            @Override
            public void onResponse(Call<StoresService.StoreGet> call, Response<StoresService.StoreGet> response) {
                int storeNumber = 0;
                storeList.clear();

                for (StoresService.Store store : response.body().stores) {
                    // Añade las tiendas al mapa
                    LatLng coords = new LatLng(store.latitude, store.longitude);

                    // Muestra un estilo de marcador u otro dependiendo en el estado de verificación de la tienda
                    if (store.verification <= VOTES_TO_DELETE_STORE){
                        // Elimina la tienda
                        deleteStore(Integer.parseInt(store.id));
                    } else if (store.verification > VOTES_TO_DELETE_STORE && store.verification < VOTES_TO_VERIFIE_STORE){
                        // La tienda tiene que ser verificada
                        marker = mMap.addMarker(new MarkerOptions().position(coords).title(store.store_name).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_validate)));

                        storeList.add(storeNumber, store);
                        storeNumber++;

                        storeId = Integer.parseInt(store.id);
                        markersHashMap.put(marker, storeId);
                    } else if (store.verification >= VOTES_TO_VERIFIE_STORE) {
                        // La tienda está verficada
                        marker = mMap.addMarker(new MarkerOptions().position(coords).title(store.store_name).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

                        storeList.add(storeNumber, store);
                        storeNumber++;

                        storeId = Integer.parseInt(store.id);
                        markersHashMap.put(marker, storeId);
                    }
                }
            }

            @Override
            public void onFailure(Call<StoresService.StoreGet> call, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "¡Error al recuperar las tiendas!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    public void upvoteStore(Integer storeID){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StoresService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        StoresService.ChinoStores chinoStores = retrofit.create(StoresService.ChinoStores.class);

        chinoStores.upvoteStore(storeID).enqueue(new Callback<StoresService.StoreAddResponse>() {
            @Override
            public void onResponse(Call<StoresService.StoreAddResponse> call, Response<StoresService.StoreAddResponse> response) {
                // Toast
                Context context = getApplicationContext();
                CharSequence text = "¡Tienda votada correctamente! :)";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }

            @Override
            public void onFailure(Call<StoresService.StoreAddResponse> call, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Error al votar la tienda!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    public void downvoteStore(Integer storeID){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StoresService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        StoresService.ChinoStores chinoStores = retrofit.create(StoresService.ChinoStores.class);

        chinoStores.downvoteStore(storeID).enqueue(new Callback<StoresService.StoreAddResponse>() {
            @Override
            public void onResponse(Call<StoresService.StoreAddResponse> call, Response<StoresService.StoreAddResponse> response) {
                // Toast
                Context context = getApplicationContext();
                CharSequence text = "¡Tienda votada correctamente! :(";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }

            @Override
            public void onFailure(Call<StoresService.StoreAddResponse> call, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Error al votar la tienda!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    public void deleteStore(Integer storeID){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StoresService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        StoresService.ChinoStores chinoStores = retrofit.create(StoresService.ChinoStores.class);

        chinoStores.deleteStore(storeID).enqueue(new Callback<StoresService.StoreAddResponse>() {
            @Override
            public void onResponse(Call<StoresService.StoreAddResponse> call, Response<StoresService.StoreAddResponse> response) {
                // Toast
                Context context = getApplicationContext();
                CharSequence text = "¡Tienda eliminada correctamente! :(";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }

            @Override
            public void onFailure(Call<StoresService.StoreAddResponse> call, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Error al eliminar la tienda!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (requestCode == FINE_LOCATION_ACCESS_REQUEST) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                //Recarga con localización activada
                finish();
                startActivity(getIntent());
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(40.416775, -3.703790), 12), 5000, null);
            }
        }
    }
}