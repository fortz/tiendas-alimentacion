package fortz.tiendasalimentacion;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public final class StoresService {
    public static final String API_URL = "http://fortz.esy.es/";

    public static class StoreGet {
        public final String status;
        public final String message;
        public final List<Store> stores;

        public StoreGet(String status, String message, List<Store> stores) {
            this.status = status;
            this.message = message;
            this.stores = stores;
        }
    }

    public static class Store {
        public String id;
        public String store_name;
        public double latitude;
        public double longitude;
        public String street;
        public String notes;
        public Integer verification;
    }

    public static class StoreAdd {

        public String store_name;
        public double latitude;
        public double longitude;
        public String street;
        public String notes;

        public StoreAdd(String store_name, double latitude, double longitude, String street, String notes) {
            this.store_name = store_name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.street = street;
            this.notes = notes;
        }
    }

    public static class StoreAddResponse {
        String status;
        String message;

        StoreAddResponse() {
            this.status = "";
            this.message = "";
        }
    }

    public interface ChinoStores {
        @GET("stores")
        Call<StoreGet> getStores();

        @POST("store")
        Call<StoreAddResponse> addStore(@Body StoreAdd storeAdd);

        @POST("upvoteStore/{storeID}")
        Call<StoreAddResponse> upvoteStore(@Path("storeID") int storeID);

        @POST("downvoteStore/{storeID}")
        Call<StoreAddResponse> downvoteStore(@Path("storeID") int storeID);

        @POST("delete/{storeID}")
        Call<StoreAddResponse> deleteStore(@Path("storeID") int storeID);
    }
}